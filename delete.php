<html>
   <head>
      <title>Deleting Record</title>
   </head>
   <body>
      <?php

		 include_once("config.php");
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);

         if(! $conn ) {
            die('Could not connect: ' . mysqli_error($conn));
         }
         echo 'Connected successfully<br />';
         
         $id = 0;
         if(isset($_GET["id"])) $id = purgeStr($_GET["id"]);
         
         mysqli_select_db( $conn, $db );
         
         $mysqli = new mysqli($dbhost, $dbuser, $dbpass, $db);
         
         if ($mysqli->query('DELETE FROM '.$table_contacts.' where id = '.$id)) {
            printf("Table $table_contacts record $id deleted successfully.<br />");
         }
         if ($mysqli->errno) {
            printf("Could not delete record $id from table: %s<br />", $mysqli->error);
         }
         
      ?>
      
      <br />
      <a href="index.php"> go home</a>
    </body>
 </html>
