<html>
   <head>
      <title>Selecting Records</title>
   </head>
   <body bgcolor="pink">
      <?php

		 include_once("config.php");
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);

         if(! $conn ) {
            die('Could not connect: ' . mysqli_error($conn));
         }
         echo 'Connected successfully<br />';
         
         $sort = 'id';
         if(isset($_GET["sort"])) $sort = purgeStr($_GET["sort"]);
         
         mysqli_select_db( $conn, $db );
         
         
         $sql = "SELECT id, title, name, submission_date, phone_private, phone_work, birthday FROM $table_contacts order by $sort";
         $retval = mysqli_query( $conn, $sql );
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error($conn) . ' <br /> <a href="createTable.php"> create Table </a>');
         }
         
         echo "<Table> <tr>".
			   "<th><a href='?sort=id'>Contact ID</a></th>".
               "<th><a href='?sort=title'>Title</a></th>".
               "<th><a href='?sort=name'>Name</a></th>".
               "<th><a href='?sort=submission_date'>submission_date</a>".
               "<th><a href='?sort=phone_private'>phone private</a>".
               "<th><a href='?sort=phone_work'>phone work</a>".
               "<th><a href='?sort=birthday'>birthday</a>".
 
               "Submission Date</a></th><th>Update</th></th><th>Delete</th></tr>";
         while($row = mysqli_fetch_array($retval)) {
            echo "<tr><td>{$row['id']}</td> :  ".
               "<td>{$row['title']}</td> ".
               "<td>{$row['name']}</td> ".
               "<td>{$row['submission_date']}</td> ".
               "<td>{$row['phone_private']}</td> ".
               "<td>{$row['phone_work']}</td> ".
               "<td>{$row['birthday']}</td> ".
               
               "<td><a href=\"update.php?id={$row['id']}&title={$row['title']}&name={$row['name']}&phone_private={$row['phone_private']}&phone_work={$row['phone_work']}&birthday={$row['birthday']}\"> update </a></td>".
               "<td><a href=\"delete.php?id={$row['id']}\"> delete </a></td>".
               "</tr>";
         } 
         echo "</Table>";
         mysqli_close($conn);
      ?>
      
      <br/><a href="insert.php"> add contact </a>
   </body>
</html>
