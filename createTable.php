<html>
   <head>
      <title>Creating MySQL Table</title>
   </head>
   <body>
      <?php
         include_once("config.php");
         $mysqli = new mysqli($dbhost, $dbuser, $dbpass);         
         if ($mysqli->query("CREATE DATABASE IF NOT EXISTS $db")) {
            printf("Database test created successfully.<br />");
         }
         if ($mysqli->errno) {
            printf("Could not create database $db: %s<br />", $mysqli->error);
         }
         
         $mysqli = new mysqli($dbhost, $dbuser, $dbpass, $db);
         
         if($mysqli->connect_errno ) {
            printf("Connect failed: %s<br />", $mysqli->connect_error);
            exit();
         }
         
         
         printf('Connected successfully.<br />');
         $sql = "CREATE TABLE IF NOT EXISTS $table_contacts( ".
            "id INT NOT NULL AUTO_INCREMENT, ".
            "title VARCHAR(256) NULL, ".
            "name VARCHAR(256) NULL, ".
            "address VARCHAR(256) NULL, ".
            "phone_private VARCHAR(32) NULL, ".
            "phone_work VARCHAR(32) NULL, ".
            "submission_date DATE, ".
            "birthday VARCHAR(32), ".
            "PRIMARY KEY ( id )); ";
         if ($mysqli->query($sql)) {
            printf("Table tutorials_tbl created successfully.<br /><a href='index.php'> go home </a>");
         }
         if ($mysqli->errno) {
            printf("Could not create table: %s<br />", $mysqli->error);
         }
         $mysqli->close();
      ?>
   </body>
</html>
